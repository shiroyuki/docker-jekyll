# Jekyll Docker Image

This is customized for general uses.

## How to create a new site

```shell
make CMD="new sample-site" OPTS="-v `pwd`:/opt -w /opt" exec
```

## How to run a dev server.

```shell
make OPTS="-v `pwd`/sample-site:/opt -w /opt" dev-server
```
