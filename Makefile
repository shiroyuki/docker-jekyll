IMAGE_TAG=shiroyuki/jekyll
WEB_CONTAINER_NAME=shiroyuki.jekyll.web
WEB_PORT=8000
OPTS=
UID=$$UID
INTERACTIVE_RUN=docker run -it -u $(UID) --rm

build:
	docker build -t $(IMAGE_TAG) .

push:
	docker push $(IMAGE_TAG)

pull:
	docker pull $(IMAGE_TAG)

exec:
	$(INTERACTIVE_RUN) \
		$(OPTS) \
		$(IMAGE_TAG) \
		jekyll $(CMD)

dev-server:
	$(INTERACTIVE_RUN) \
		$(OPTS) \
		-p $(WEB_PORT):4000 \
		--name $(WEB_CONTAINER_NAME) \
		$(IMAGE_TAG) \
		jekyll serve --host 0.0.0.0

sample-new-site:
	make CMD="new sample-site" OPTS="-v `pwd`:/opt -w /opt" exec

sample-run:
	make OPTS="-v `pwd`/sample-site:/opt -w /opt" dev-server
